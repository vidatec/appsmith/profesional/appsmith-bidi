{
	"id": {{crypto.randomUUID()}},
	"title": {{InputTitle.text}},
	"description": {{InputDescription.text}},
	"order": {{InputOrder.text}},
	"institution_id": {{SelectInstitution.selectedOptionValue}},
	"enable": {{CheckboxEnable.isChecked}}
}