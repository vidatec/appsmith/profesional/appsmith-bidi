export default {
	get: async () => {
		try{
			await catalog_book_deleted.run();
		}catch(error){
			return showAlert(`${catalog_book_deleted.data.message}`,'error')
		}
	}
}